#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <SimpleTimer.h>

// Firebase settings
#define FIREBASE_HOST "fir-demoesp8266.firebaseio.com"
#define FIREBASE_AUTH "24kVq2Qtl0JNkCFD2iQFLgVFQJ4AlCU4BtrQ5Hv7"

//WiFi settings
#define WIFI_SSID "AndroidAP"
#define WIFI_PASSWORD "ipad14567"

//Timer
SimpleTimer timer;
unsigned long startTime=0;

// Ports
int trigger = D4;
int echo = D3;

// Values
bool isOn = false;
int n = 0;

// Ultrasonic values
long duration = 0;
float distance = 0.0;

// Measure function
void measure() {
  isOn = Firebase.getBool("isOn");

  unsigned long currentTime = millis();
  unsigned long elapsedTime = (currentTime - startTime) / 1000;

  if (isOn) {
    digitalWrite(trigger, LOW);
    delay(5);
    digitalWrite(trigger, HIGH);
    delay(10);
    digitalWrite(trigger, LOW);

    duration = pulseIn(echo, HIGH);
    distance = (duration/2) / 29.1;

    Firebase.setFloat("measurements/" + String(n) + "/distance", distance);
    Firebase.setInt("measurements/" + String(n) + "/elapsedTime", elapsedTime);

    n++;
  }

  Serial.print("Is on ");
  Serial.println(isOn);
}


void setup() {
  Serial.println("Setup");
  Serial.begin(9600);
  startTime = millis();

  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);

  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.print("Connected ");
  Serial.println(WiFi.localIP());

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

  Firebase.setBool("test", true);

  timer.setInterval(250L, measure);
}

void loop(){
  timer.run();
}
